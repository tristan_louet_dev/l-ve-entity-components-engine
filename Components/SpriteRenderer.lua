local SpriteRenderer = LECEngine.Core.OOImplementation.newClassInheritsFrom(LECEngine.GraphicComponent)

function SpriteRenderer.new(p_loadedImage)
	local sr = SpriteRenderer:create()
	sr:init()
	sr.sprite = p_loadedImage
	return sr
end

function SpriteRenderer:init()
	SpriteRenderer:superClass().init(self)
end

function SpriteRenderer:draw()
	love.graphics.push()
	love.graphics.translate(self.transform.position:splittedXY())
	love.graphics.rotate(self.transform.rotation)
	love.graphics.scale(self.transform.scale:splittedXY())
	love.graphics.draw(self.sprite, -self.sprite:getWidth() / 2, -self.sprite:getHeight() / 2)
	love.graphics.pop()
end

return SpriteRenderer
