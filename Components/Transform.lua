-- A Transform is the vector2 position and scale of the entity, and its rotation in radians
local Vector2 = LECEngine.Utils.Vector
local cos = math.cos
local sin = math.sin
local PI = math.pi

local Transform = LECEngine.Core.OOImplementation.newClassInheritsFrom(LECEngine.LogicComponent)

function Transform.new(p_position, p_rotation, p_scale)
	local p = Transform:create()
	p.position = p_position
	p.rotation = p_rotation
	p.scale = p_scale
	p._name = "transform"
	p:init()
	return p
end

function Transform:init()
	Transform:superClass().init(self)
	if not self.position then
		self.position = Vector2.new()
	end
	if not self.rotation then
		self.rotation = 0.0
	end
	self:_updateForwardVector()
	if not self.scale then
		self.scale = Vector2.one()
	end
end

function Transform:translate(p_fstParam, p_sndParam)
	if not p_sndParam then
		self.position = self.position:add(p_fstParam)
	else
		self.position = self.position:add(Vector2.new(p_fstParam, p_sndParam))
	end
end

function Transform:rotate(p_radianAngle)
	self.rotation = (self.rotation + p_radianAngle) %  (2 * PI)
end

function Transform:faceTo(p_vector)
	local x = p_vector.x - self.position.x
	local y = -(p_vector.y - self.position.y)
	self.rotation = math.rad(math.fmod(360 - (math.deg(math.atan2(y, x)) - 90), 360))
end

function Transform:scaleBy(p_scaleFactor)
	self.scale = self.scale:mul(p_scaleFactor)
end

function Transform:forwardVector()
	return self.forward
end

function Transform:_updateForwardVector()
	self.forward = Vector2.new(sin(self.rotation), -cos(self.rotation)):normalized()
end

function Transform:update(p_dt)
	self:_updateForwardVector()
end

return Transform
