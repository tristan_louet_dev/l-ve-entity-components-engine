local GraphicComponent = LECEngine.Core.OOImplementation.newClassInheritsFrom(LECEngine.Core.Component)

GraphicComponent._instancesCount = 0

function GraphicComponent.new()
	local gc = GraphicComponent:create()
	gc:init()
	return gc
end

function GraphicComponent:init()
	GraphicComponent:superClass().init(self)
	GraphicComponent._instancesCount = GraphicComponent._instancesCount + 1
	self._id = GraphicComponent._instancesCount
	self._layer = 0
end

function GraphicComponent:getId()
	return self._id
end

function GraphicComponent:draw()
	error ("Unimplemented draw method for GraphicComponent #" .. self:getId())
end

function GraphicComponent:setLayer(p_layer)
	if p_layer < 0 then
		error ("Negative layers unsupported (trying to set GraphicComponent #" .. self:getId() .. "'s layer to " .. p_layer .. ")")
	end
	if self:isAttached() then
		LECEngine.Core.GraphicSystem.get():changeLayer(self, p_layer)
	end
	self._layer = p_layer
end

function GraphicComponent:getLayer()
	return self._layer
end

return GraphicComponent
