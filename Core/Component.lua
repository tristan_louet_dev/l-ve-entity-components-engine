local Component = LECEngine.Core.OOImplementation.newClass()

function Component.new()
	--we don't do anything else but we wrap :create() in a
	--.new() function for API consistency
	local c = Component:create()
	c._name = nil
	return c
end

function Component:init()
	--nothing more to do
end

function Component:getType()
	-- A type of component is uniquely represented by a class derivated
	--from Component or one of its subclasses
	return self.class()
end

function Component:registerEntity(p_entity)
	self.entity = p_entity
	--We add some practical shortcuts
	self.transform = p_entity.transform
end

function Component:getEntity()
	return self.entity
end

function Component:isAttached()
	return self:getEntity() ~= nil
end

return Component
