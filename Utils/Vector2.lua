local OO = LECEngine.Core.OOImplementation
local Vector2 = OO.newClass()

local pow = math.pow
local sqrt = math.sqrt
local atan2 = math.atan2

function Vector2.new(p_x, p_y)
	local v = Vector2:create()
	v:set(p_x, p_y)
	return v
end

function Vector2:clone()
	return Vector2.new(self.x, self.y)
end

function Vector2:set(p_x, p_y)
	if not p_x then
		p_x = 0
	end
	if not p_y then
		p_y = 0
	end
	self.x = p_x
	self.y = p_y
end

function Vector2:equals(p_other)
	return (self.x == p_other.x) and (self.y == p_other.y)
end

function Vector2:nequals(p_other)
	return not self:equals(p_other)
end

function Vector2:neg()
	return Vector2.new(-self.x, -self.y)
end

function Vector2:add(p_other)
	return Vector2.new(self.x + p_other.x, self.y + p_other.y)
end

function Vector2:sub(p_other)
	return self:add(p_other:neg())
end

function Vector2:mul(p_scalar)
	return Vector2.new(self.x * p_scalar, self.y * p_scalar)
end

function Vector2:div(p_scalar)
	return Vector2.new(self.x / p_scalar, self.y / p_scalar)
end

function Vector2:sqrNorm()
	return pow(self.x, 2) + pow(self.y, 2)
	end

function Vector2:norm()
	return sqrt(self:sqrNorm())
end

function Vector2:normalized()
	local n = self:norm()
	return Vector2.new(self.x / n, self.y / n)
end

function Vector2:sqrDistance(p_other)
	return pow(self.x - p_other.x, 2) + pow(self.y - p_other.y, 2)
end

function Vector2:distance(p_other)
	return sqrt(self:sqrDistance(p_other))
end

-- returns the angle in radians
function Vector2:angle(p_other)
	return atan2(self.y, self.x) - atan2(p_other.y, p_other.x)
end

function Vector2:dot(p_other)
	return self.x * p_other.x + self.y * p_other.y
end

function Vector2:scale(p_other)
	return Vector2.new(self.x * p_other.x, self.y * p_other.y)
end

function Vector2:splittedXY()
	return self.x, self.y
end

function Vector2.zero()
	return Vector2.new()
end

function Vector2.one()
	return Vector2.new(1, 1)
end

function Vector2.up()
	return Vector2.new(0, -1)
end

function Vector2.down()
	return Vector2.new(0, 1)
end

function Vector2.right()
	return Vector2.new(1, 0)
end

function Vector2.left()
	return Vector2.new(-1, 0)
end

return Vector2
